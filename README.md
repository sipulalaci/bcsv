# Eskimos vs Researchers
Egy csoportos projekt az egyik egyetemi tárgyunkhoz, aminek a célja egy multiplayer játék készítése volt. Programozási nyelv Java és elvárás volt, hogy grafikus felülettel rendelkezzen.

Az első 6-7 hét csak tervezésről és dokumentálásról szólt. Tervezés alatt meg kellett határoznunk a különböző követelményeket(funkcionális, nem-funkcionális és stb.), use-caseket kellet megfogalmazni és több féle diagramot(szekvencia, osztály és állapot) kellett készítenünk. 

A 7. héttől kezdve a fejlesztésre került át a hangsúly, viszont még volt egy kis tervezés is hátra. A különböző osztályok és metódusok megtervése. Amint ezzel megvoltunk, jöhetett a programozás. Először a mögöttes logikát készítettük el, majd miután ezzel megvoltunk JavaFX segítségével készítettük a grafikus felületet.

A 10. héttől a tesztek is megfogalmazásra kerültek, majd hozzá lettek adva a prototípushoz.
